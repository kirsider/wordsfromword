import Utils from '../../services/Utils.js';

let Word = {
    render: async () => {
        const request = Utils.parseRequestURL();
        const wid = request.id;

        const word = (await firebase.database().ref('allwords/' + wid).once('value')).val();
        
        if (!word) {
            return `
            <div class="form-box">
                <div class="form-header-box">
                    <p>Слова не знойдзена :(</p>
                </div>
            </div>
            `;
        }

        let html = 
        `
            <div class="action-box">
                <div class="form-header-box">
                    <p id="word">${word}</p>
                </div>
                <input id="word-input" type="text" class="word-input"></input>
                <button id="send-btn" type="button" class="send-btn">Адправіць</button>
                <a href="/#/rating/${wid}" class="rating-link">Лепшыя вынікі</a>
                <div id="user-words" class="grid-wrapper">
                </div>
            </div>
        `;
        return html;
    },

    after_render: async () => {
        const wid = Utils.parseRequestURL().id;
        const word = document.getElementById("word").innerHTML;
        const send_btn = document.getElementById("send-btn");
        const user_words = document.getElementById("user-words");

        let saved_words = (await firebase.database().ref('userwords/' + firebase.auth().currentUser.uid + '/' + wid).once('value')).val();
        
        if (saved_words) {
            saved_words = Object.values(saved_words);

            for (let w of saved_words) {
                let div = document.createElement("div");
                div.setAttribute("class", "word-wrapper");
                div.innerHTML = w;
                user_words.appendChild(div);
            }
        }

        send_btn.addEventListener("click", (e) => {
            const user_word = document.getElementById("word-input").value;
            if (checkInput(user_word)) {
                $.ajax({
                    type: "POST",
                    url: "https://corpus.by/VoicedElectronicGrammaticalDictionary/api.php",
                    data:{
                    "searchRequest": user_word,
                    "category": "назоўнік",
                    "sbm1987": 1,   
                    "maximumResults": 1
                },
                success: async function(msg){
                    let result = JSON.parse(msg); 
                    if (result["resultArr"]["no_results"]) {
                        Utils.createSnackbar("Ваша слова ў слоўніке не знойдзена");
                        return;
                    } 
                    if (result["resultArr"]["sbm1987"][0]) {
                        let current_words = (await firebase.database().ref('userwords/' + firebase.auth().currentUser.uid + '/' + wid).once('value')).val();
                        current_words = current_words ? Object.values(current_words) : null;
                        if (current_words && current_words.includes(user_word)) {
                            Utils.createSnackbar("Гэта слова ўжо знойдзена");
                            return;
                        }
                        let div = document.createElement("div");
                        div.setAttribute("class", "word-wrapper");
                        div.innerHTML = user_word;
                        user_words.appendChild(div);

                        firebase.database().ref('userwords/' + firebase.auth().currentUser.uid + '/' + wid).push(user_word);
                        Utils.createSnackbar("Ёсць такое слова!");
                    }
                }
                });
            }
        });
    
        function checkInput(w) {
            if (w.length < 2) {
                Utils.createSnackbar("У вашым слове павінна быць 2 і больш літары");
                return false;
            }
            if (w.length > word.length) {
                Utils.createSnackbar("У вашым слове больш літар, чым у прапанованым слове");
                return false;
            }
            for (let ch of w) {
                const re = new RegExp(`${ch}`, 'g');
                if ((w.match(re) || []).length > (word.match(re) || []).length) {
                    Utils.createSnackbar("У вашым слове ёсть сімвалы, якіх няма у прапанованым слове");
                    return false;
                }
            }
            return true;
        }
    }

}

export default Word;