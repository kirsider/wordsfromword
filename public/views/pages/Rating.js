import Utils from '../../services/Utils.js';

let Rating = {
    render: async () => {
        const request = Utils.parseRequestURL();
        const wid = request.id;

        const word = (await firebase.database().ref('allwords/' + wid).once('value')).val();

        const users_results = (await firebase.database().ref('userwords/').once('value')).val();
        const uids = Object.keys(users_results);

        const users = (await firebase.database().ref('users/').once('value')).val();
        
        const top_results = [];

        for (let uid of uids) {
            let words = users_results[uid][wid];
            
            if (words) {
                top_results.push([users[uid], Object.keys(words).length]);
            }
        }
        top_results.sort((a, b) => b[1] - a[1]);

        let items = ``;
        let cnt = 1;
        for (let item of top_results) {
            let label = `
            <label class="answer-container">
                <b>${cnt + ". "}</b> ${item[0]} --- ${item[1]}
            </label> 
            `;
            items += label;
            cnt++;
        }

        let html = `
        <div class="sign-box">
            <div class="top-par">Лепшыя па слову "${word}"</div>
            ${items}
        </div>
        `;

        return html;
    },

    after_render: async () => {

    }
}

export default Rating;