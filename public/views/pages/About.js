let About = {
    render: async () => {
        return `
        <div class="form-box">
            <div class="form-header-box">
                <p>Аб праекце</p>
                <div class="description-box">
                Сайт уяўляе сабой аднастаронкавы дадатак (Single Page Application, SPA). 
                Праект напісаны на Javascript. У праекце выкарыстоўваюцца сэрвісы Firebase 
                (аўтэнтыфікацыя карыстальнікаў, база дадзеных). Для пачатку карыстальнік павінен 
                аўтарызавацца альбо стварыць новы рахунак. Пасля ўваходу карыстальнік выбірае 
                слова з прадстаўленых і пераходзіць на старонку складання слоў. На гэтай старонцы 
                карыстальнік можа ўбачыць ужо раней складзеныя ім словы. Таксама карыстальнік 
                можа паспрабаваць скласці новыя словы. Націснуўшы на кнопку «Адправіць» 
                складзенае слова спачатку правяраецца на адэкватнасць (колькі літар у слове, 
                якія сімвалы ўведзеныя), потым адпраўляецца запыт да  
                <a href="https://corpus.by/VoicedElectronicGrammaticalDictionary/?lang=be">сэрвісу</a> для праверкі 
                наяўнасці слова ў слоўніку. Калі такое маецца і слова раней не было разгадана, 
                яно дадаецца ў спіс разгаданых слоў. Таксама карыстальнік можа перайсці па спасылцы 
                «Лепшыя вынікі» і даведацца спіс лепшых па канкрэтным слову. У падвале кожнай старонцы 
                размешчаны 3 спасылкі ( «Хатняя», «Аб праекце», «Напісаць аўтару»). На старонцы 
                «Аб праекце» карыстальнік можа даведацца падрабязней пра праект і яго мэтах. 
                Націснуўшы на «Напісаць аўтару» карыстальнік можа адправіць e-mail аўтару праекта 
                з пажаданнямі, падзякамі і г.д.
                </div>
            </div>
        </div>
        `;
    },
    
    after_render: async () => {}
}

export default About;