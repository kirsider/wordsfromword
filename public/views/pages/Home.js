import Utils from '../../services/Utils.js';

let Home = {
    render: async () => {
        let view = `
        <div class="templates-box">
            <h2>Усе словы</h2>
            <div id="all-words" class="grid-wrapper">
            </div>  
        </div>
        `;
        return view;
    }, 
    
    after_render: async () => {
        const allWords = document.getElementById("all-words");

        const snapshot = await firebase.database().ref('allwords').once('value');
        const dictWords = snapshot.toJSON();
        let links = [];

        for (const kword of Object.keys(dictWords)) {
            const div = document.createElement("div");
            div.setAttribute("class", "word-wrapper");

            const a = document.createElement("a");
            a.innerHTML = dictWords[kword];
            a.href = "/#/word/" + kword;
            links.push(a);

            div.appendChild(a);
            allWords.appendChild(div);
        }
        
        firebase.auth().onAuthStateChanged(async firebaseUser => { 
            if (firebaseUser) {
                firebase.database().ref('users/' + firebase.auth().currentUser.uid).set(firebase.auth().currentUser.email);
                for (let a of links) {
                    a.addEventListener("click", (e) => {})
                }
            }
            else {
                for (let a of links) {
                    a.addEventListener("click", (e) => {
                        e.preventDefault();
                        Utils.createSnackbar("Для пачатку трэба аўтарызавацца")
                    })
                }
            }
        })
    }

}

export default Home;