import Utils from '../../services/Utils.js';

let SignUp = {
    render: async () => {
        return `
        <div class="sign-box">
            <p>Рэгiстрацыя</p>
            <form class="sign-form">
                <input id="email_input" type="text" name="email" placeholder="Email"><br>
                <input id="pass_input" type="password" name="password" placeholder="Пароль"><br>
                <input id="confirm_pass_input" type="password" name="confirm" placeholder="Пацвердзіць пароль"><br>
                <button id="signup_btn" type="button">Зарэгістравацца</button>
            </form>
        </div>
        `;
    },

    after_render: async () => {
        const signin_btn = document.getElementById("signup_btn");

        signin_btn.addEventListener("click", () => {
            event.preventDefault();

            const email_input = document.getElementById("email_input");
            const pass_input = document.getElementById("pass_input");
            const confirm_pass_input = document.getElementById("confirm_pass_input");

            if (pass_input.value !== confirm_pass_input.value) {
                Utils.createSnackbar("'Пароль' і 'Пацвердзіць пароль' не супадаюць!");
            } else if (email_input.value === '' || pass_input.value === '' || confirm_pass_input.value === '') {
                Utils.createSnackbar("Усе палі павінны быць запоўненыя!");
            } else {
                const auth = firebase.auth();
                const promise = auth.createUserWithEmailAndPassword(email_input.value, pass_input.value);
                promise.catch(e => Utils.createSnackbar(e.message));
                
                window.location.href = '/#/';
            }

        })
    }
}

export default SignUp;